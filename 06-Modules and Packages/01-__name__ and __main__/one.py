print('hello')

def myfunc():
    print('FUNC( IN ONE.PY')

print('TOP LEVEL IN ONE.PY')

# if __name__ == '__main__':
#     print('ONE.PY is being run directly!')
# else:
#     print('ONE.PY has been imported!!')

def func1():
    print('Func1')

def func2():
    print('Func2')

if __name__ == '__main__':
    func1()
    func2()