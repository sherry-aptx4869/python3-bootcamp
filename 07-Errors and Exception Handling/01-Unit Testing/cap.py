def cap_text(text):
    '''
    Input a text and capitalize it
    '''
    return text.title()