'''
A very simple script using pylint simple1.py -r y
'''

def myfunc():
    '''
    A simple function
    '''
    first = 1
    second = 2
    print(f'{first} \n {second}')

myfunc()
